import Component from '../lib/component.js';
import store from '../store/index.js';

export default class Done extends Component {
    constructor() {
        super({
            store,
            element: document.querySelector('.done-list .list')
        });
    }

    render() {
        let self = this;
        self.element.innerHTML = store.state.items.done.map(item => {
            return `<li class="list-item">${item.task} ${item.date} ${item.name}</li>`
        }).join('')
    };
};
