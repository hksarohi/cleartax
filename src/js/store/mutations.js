export default {
    addStarted(state, payload) {
        state.items.started.push(payload);
        return state;
    },
    addPlanned(state, payload) {
        state.items.planned.push(payload);
        return state;
    },
    addDone(state, payload) {
        state.items.done.push(payload);
        return state;
    }
};
