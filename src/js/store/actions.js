export default {
    addStarted(context, payload) {
        context.commit('addStarted', payload);
    },
    addPlanned(context, payload) {
        context.commit('addPlanned', payload);
    },
    addDone(context, payload) {
        context.commit('addDone', payload);
    }
};
