import store from './store/index.js';

// Load up components
import Planned from './components/planned.js';
import Started from './components/started.js';
import Done from './components/done.js';

//const btns = document.querySelectorAll('.members-list button');
const form = document.querySelector('form');

/*btns.forEach(item => {
    item.addEventListener('click', function () {
        form.scrollIntoView();
    })
});
*/

form.addEventListener('submit', evt => {
    evt.preventDefault();

    let task = document.querySelector('input[name="task"]').value.trim();
    let date = document.querySelector('input[name="date"]').value.split('-').reverse().join('/');
    let list = document.querySelector('select[name="list"]').value;
    let assignee = document.querySelector('select[name="assignee"]').value;
    let obj = {task, date, name: assignee};
    if (list === 'planned') {
        store.dispatch('addPlanned', obj);
    } else if (list === 'started') {
        store.dispatch('addStarted', obj);
    } else if (list === 'done') {
        store.dispatch('addDone', obj);
    }
});

// Instantiate components
const plannedInstance = new Planned();
const startedInstance = new Started();
const doneInstance = new Done();

// Initial renders
plannedInstance.render();
startedInstance.render();
doneInstance.render();
